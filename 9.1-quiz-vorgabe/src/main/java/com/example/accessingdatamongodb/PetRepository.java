package com.example.accessingdatamongodb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PetRepository extends MongoRepository<Pet, String> {

	public Pet findByName(String name);




}
