package com.example.accessingdatamongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Document(collection = "person")
public class Person {

    @Id
    private String id;
    private String firstname;
    private String lastname;


    private List<Pet> pets;
    public Person() {
        pets = new ArrayList<Pet>();
        firstname = "";
        lastname = "";
    }


    public Person(String firstName, String lastName) {
        this.pets = new ArrayList<Pet>();


        this.firstname = firstName;
        this.lastname = lastName;
    }
    public Person(String firstName, String lastName, Pet pet) {
        this.pets = new ArrayList<Pet>();

        this.pets.add(pet);
        this.firstname = firstName;
        this.lastname = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pets=" + pets +
                '}';
    }
}

