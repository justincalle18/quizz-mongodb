package com.example.accessingdatamongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.AggregateIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.logging.Logger;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Controller
public class ViewController {
    private static Logger logger = Logger.getLogger("ViewController");
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private PetRepository petRepository;
    String connectionString;
    MongoClient mongoClient;
    @PostConstruct
    public void init() {
        connectionString = "mongodb://root:1234@localhost:27017";
        mongoClient = MongoClients.create(connectionString);
    }

    @GetMapping("/")
    public String index(Model model) {
         // get !!
        //    model.addAttribute("customers", mongoClient.getDatabase("customerdb").getCollection("customer").find());
        model.addAttribute("customers", customerRepository.findAll());
        logger.info( "hey----> "+customerRepository.findAll());
        return "home";
    }

    @GetMapping("/pets")
    public String pets(Model model) {

        Person person = customerRepository.findById("637965a5b376b77fd3a2f094").get();
        model.addAttribute("pets", person.getPets());
        logger.info( "pets----> "+person.getPets());
        return "home";
    }

    @GetMapping("/add-customer")
    public String showAddCustomerUpForm(Model model, Person customer) {
        model.addAttribute("customers", customerRepository.findAll() );
        model.addAttribute("customer", new Person());
        return "add-customer";
    }

    @PostMapping("/add-customer")
    public String addCustomer(Person customer, Model model)
    {
        logger.info("customer : "+customer);
        customerRepository.save(customer);
        model.addAttribute("customers", customerRepository.findAll());
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") String id, Model model) {
        Person customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("customers", customerRepository.findAll() );
        model.addAttribute("customer", customer);
        return "upd-customer";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") String id, @Valid Person customer,
                             BindingResult result, Model model) {
        if (result.hasErrors()) {
            customer.setId(id);
            return "upd-customer";
        }

        customerRepository.save(customer);
        model.addAttribute("customers", customerRepository.findAll());
        return "home";
    }


    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") String id, Model model) {
        Person customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        customerRepository.delete(customer);
        model.addAttribute("customers", customerRepository.findAll());
        return "home";
    }


    @GetMapping("/projections")
    public String projections(Model model) {
        model.addAttribute("customers", customerRepository.findNameAndExcludeId());
        // logger.info( "hey----> "+customerRepository.findNameAndExcludeId());
        return "projections";
    }


    @GetMapping("/aggregations")
    public String aggregations(Model model) {
        model.addAttribute("customers", customerRepository.findNameAndExcludeId());

        GroupOperation groupByStateAndSumPop = group("state")
                .sum("pop").as("statePop");



        // logger.info( "hey----> "+customerRepository.findNameAndExcludeId());
        return "aggregation";
    }
}
