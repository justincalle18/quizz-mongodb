package com.example.accessingdatamongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

// https://spring.io/guides/gs/accessing-data-mongodb/
//        https://www.baeldung.com/spring-data-mongodb-projections-aggregations
//        https://stackabuse.com/spring-data-mongodb-guide-to-the-aggregation-annotation/
//https://docs.spring.io/spring-data/mongodb/docs/current/api/org/springframework/data/mongodb/repository/Aggregation.html
@SpringBootApplication
public class AccessingDataMongodbApplication implements CommandLineRunner {

	@Autowired
	private CustomerRepository repository;
	private static Logger logger = Logger.getLogger("AccessingDataMongodbApplication");
	public static void main(String[] args) {
		SpringApplication.run(AccessingDataMongodbApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		repository.deleteAll();

		// a couple of pets: dogs
		Pet max = new Pet("622112d71f9dac417b84227d","Max");
		Pet bello = new Pet("622112d71f9dac417b84227e", "Bello");
		// cats
		Pet belinda = new Pet("622112d71f9dac417b84227f","Belinda");
		Pet natasha = new Pet("622112d71f9dac417b842270","Natasha");

		// save a couple of persons
		Person alice = new Person("Alice", "Smith");
		alice.setPet(new Pet("622112d71f9dac417b842281","fiore"));
		alice.setPet(new Pet("622112d71f9dac417b842282","sole"));
		repository.save(alice);

		Person bob = new Person("Bob", "Smith");
		bob.setPet(new Pet("1123123", "leopardo"));
		bob.setPet(belinda);
		bob.setPet(natasha);

		repository.save(bob);
		repository.save(new Person("John", "Doe", bello));



		// fetch all customers
		System.out.println("Persons found with findAll():");
		System.out.println("-------------------------------");
//		System.out.println(repository.findAll());
		logger.info("info "+repository.findAll());
		for (Person customer : repository.findAll()) {
			System.out.println(customer);

		}
		System.out.println();

		// fetch an individual customer
		System.out.println("Persons found with findByFirstName('Alice'):");
		System.out.println("--------------------------------");
		System.out.println(repository.findByFirstname("Alice"));

		System.out.println("Persons found with findByLastName('Smith'):");
		System.out.println("--------------------------------");
		for (Person customer : repository.findByLastname("Smith")) {
			System.out.println(customer);
			for (Pet pet : customer.getPets()) {
				System.out.println("pet name: "+pet.getName());
			}
		}

	}

}
