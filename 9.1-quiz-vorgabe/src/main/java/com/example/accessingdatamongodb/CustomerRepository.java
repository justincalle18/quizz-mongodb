package com.example.accessingdatamongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface CustomerRepository extends MongoRepository<Person, String> {

	public Person findByFirstname(String firstname);
	public List<Person> findByLastname(String lastname);

	// https://www.baeldung.com/spring-data-mongodb-projections-aggregations
	@Query(value="{}", fields="{firstname : 1,  _id : 0}")
	List<Person> findNameAndExcludeId();



}
