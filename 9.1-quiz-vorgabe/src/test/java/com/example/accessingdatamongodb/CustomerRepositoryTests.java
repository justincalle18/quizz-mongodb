package com.example.accessingdatamongodb;
/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;

@SpringBootTest
public class CustomerRepositoryTests {

	@Autowired
	CustomerRepository repository;

	Person dave, oliver, carter;

	@BeforeEach
	public void setUp() {

		repository.deleteAll();

		dave = repository.save(new Person("Dave", "Matthews"));
		oliver = repository.save(new Person("Oliver August", "Matthews"));
		carter = repository.save(new Person("Carter", "Beauford"));
	}

	@Test
	public void setsIdOnSave() {

		Person dave = repository.save(new Person("Dave", "Matthews"));

		assertThat(dave.getId()).isNotNull();
	}

	@Test
	public void findsByLastName() {

		List<Person> result = repository.findByLastname("Beauford");

		assertThat(result).hasSize(1).extracting("firstname").contains("Carter");
	}

	@Test
	public void findsByExample() {

		Person probe = new Person(null, "Matthews");

		List<Person> result = repository.findAll(Example.of(probe));

		assertThat(result).hasSize(2).extracting("firstname").contains("Dave", "Oliver August");
	}
}
